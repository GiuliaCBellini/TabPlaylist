INSERT INTO public."Playlist"(
	"Canzoni", "Artisti", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica")
	VALUES ('Trottolino Amoroso','Re dei Wustel','4.33', 'pop', '1', 'Canta Che Ti Passa');

INSERT INTO public."Playlist"(
	"Canzoni", "Artisti", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica")
	VALUES ('Sali e Scendi', 'Rihanna', '3.45', 'lirico','5', 'Sony');
	
INSERT INTO public."Playlist"(
	"Canzoni", "Artisti", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica")
	VALUES ('Il pistacchio grasso', 'Pistacchione',  '2.45', 'rap','2', 'Tanto Per Cantà');
	
INSERT INTO public."Playlist"(
	"Canzoni", "Artisti", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica")
	VALUES ('100 Giorni di Abbuffata', 'Calirano', '2.56', 'country','4', 'Dietro Casa');
	
INSERT INTO public."Playlist"(
	"Canzoni", "Artisti", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica")
	VALUES ('Il Ballo Del Cavallo', 'DJ Frittata', '3.55', 'punk-rock', '3', 'Furetti Bros');

INSERT INTO public."Playlist"(
	"Canzoni", "Artisti", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica")
	VALUES ('Occhi di Gatto', 'Cristiana DAvena', '4.12', 'metal', '6', 'Grillo Parlante');
 
--Inserimento righe all'interno della tabella Playlist--