-- Table: public.Playlist

-- DROP TABLE IF EXISTS public."Playlist";

CREATE TABLE IF NOT EXISTS public."Playlist"
(
    "Canzoni" character varying COLLATE pg_catalog."default" NOT NULL,
    "Artisti" character varying COLLATE pg_catalog."default",
    "DurataMinutiCanzone" double precision,
    "Genere" character varying COLLATE pg_catalog."default",
    "NumeroCanzone" character varying COLLATE pg_catalog."default",
    "CasaDiscografica" character varying COLLATE pg_catalog."default",
    CONSTRAINT "Playlist_pkey" PRIMARY KEY ("Canzoni")
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."Playlist"
    OWNER to postgres;

COMMENT ON TABLE public."Playlist"
    IS 'Nome Tabella';

COMMENT ON COLUMN public."Playlist"."Canzoni"
    IS 'nome delle canzoni inserite';

COMMENT ON COLUMN public."Playlist"."Artisti"
    IS 'nome dei cantanti';

COMMENT ON COLUMN public."Playlist"."DurataMinutiCanzone"
    IS 'durata della canzone in minuti';

COMMENT ON COLUMN public."Playlist"."Genere"
    IS 'genere canzone';

COMMENT ON COLUMN public."Playlist"."NumeroCanzone"
    IS 'disposizione numerica della canzone nella playlist';

COMMENT ON COLUMN public."Playlist"."CasaDiscografica"
    IS 'nome della casa discografica ';