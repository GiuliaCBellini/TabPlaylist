CREATE TABLE PLAYLIST(
Canzoni                        character varying(30) NOT NULL,
Artista                        character varying(30),
DurataMinutiCanzone            double precision(5),
Genere                         character varying(20),
NumeroCanzone                  character varying(5),
CasaDiscografica               character varying(30));


INSERT INTO public."Playlist" (
"Canzoni", "Artista", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica") VALUES (
'Trottolino amoroso'::character varying, 'Re dei Wustel'::character varying, '4.33'::double precision, 'pop'::character varying, '1'::character varying, 'Canta Che Ti Passa'::character varying)
 returning "Canzoni";

INSERT INTO public."Playlist" (
"Canzoni", "Artista", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica") VALUES (
'Sali e Scendi'::character varying, 'Rihanna'::character varying, '3.45'::double precision, 'lirico'::character varying, '3'::character varying, 'Sony'::character varying)
 returning "Canzoni";

 INSERT INTO public."Playlist" (
"Canzoni", "Artista", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica") VALUES ('
'Il pistacchio grasso'::character varying, 'Pistacchione'::character varying, '2.45'::double precision, 'rap'::character varying, '2'::character varying, 'Tanto Per Cantà'::character varying)
returning "Canzoni";

INSERT INTO public."Playlist" (
"Canzoni", "Artista", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica") VALUES (
'100 Giorni di Abbuffata'::character varying, 'Calirano'::character varying, '2.56'::double precision, 'country'::character varying, '4'::character varying, 'Dietro Casa'::character varying)
 returning "Canzoni";

INSERT INTO public."Playlist" (
"Canzoni", "Artista", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica") VALUES (
'Il Ballo Del Cavallo'::character varying, 'DJ Frittata'::character varying, '3.55'::double precision, 'punk-rock'::character varying, '3'::character varying, 'Furetti Bros'::character varying)
 returning "Canzoni";

INSERT INTO public."Playlist" (
"Canzoni", "Artista", "DurataMinutiCanzone", "Genere", "NumeroCanzone", "CasaDiscografica") VALUES (
'Occhi di Gatto'::character varying, 'Cristiana D'Avena'::character varying, '4.12'::double precision, 'metal'::character varying, '3'::character varying, 'Grillo Parlante'::character varying)
 returning "Canzoni";


 

SELECT * FROM public."Playlist"
ORDER BY "Canzoni" ASC 

